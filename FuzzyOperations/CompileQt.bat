cd "C:\Program Files (x86)\Microsoft Visual Studio\Shared\Python36_64\Scripts"
pyuic5.exe %~dp0GUI\MainWindowBase.ui -o %~dp0GUI\MainWindowBase.py
pyuic5.exe %~dp0GUI\AddSetDialogBase.ui -o %~dp0GUI\AddSetDialogBase.py
pyuic5.exe %~dp0GUI\CustomDialogBase.ui -o %~dp0GUI\CustomDialogBase.py
pyuic5.exe %~dp0GUI\LRDialogBase.ui -o %~dp0GUI\LRDialogBase.py
pyuic5.exe %~dp0GUI\OpDialogBase.ui -o %~dp0GUI\OpDialogBase.py
pyuic5.exe %~dp0GUI\ItemWidgetBase.ui -o %~dp0GUI\ItemWidgetBase.py
pyrcc5.exe %~dp0Resources\Resource.qrc -o %~dp0Resource_rc.py