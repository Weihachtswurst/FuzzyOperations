import xml.etree.ElementTree as ET
import FuzzyNumber as Fuzz
import numpy as np

def loadElement(element):
    color = element.get("color", "255;0;255").split(';')

    if element.tag == "lr":
        return Fuzz.LRFuzzyNumber(element.get("name", "?"), float(element.get("m", "0")), float(element.get("alpha", "1")), float(element.get("beta", "1")), (float(color[0]),float(color[1]),float(color[2])))
    else:
        universe = []
        probability = []

        for tuple in element:
            universe.append(float(tuple.find("x").text))
            probability.append(float(tuple.find("y").text))

        return Fuzz.FuzzyNumber(element.get("name", "?"), universe, probability, (float(color[0]),float(color[1]),float(color[2])))

def saveElement(fuzzyNumber, nodeNumbers):
    if type(fuzzyNumber) == Fuzz.LRFuzzyNumber:
        ET.SubElement(nodeNumbers[0], "lr", { "name" : fuzzyNumber.name, "m" : str(fuzzyNumber.m), "alpha" : str(fuzzyNumber.alpha), "beta" : str(fuzzyNumber.beta), "color" : "{};{};{}".format(fuzzyNumber.color[0], fuzzyNumber.color[1], fuzzyNumber.color[2])})
    else:
        nodeCustom = ET.SubElement(nodeNumbers[0], "custom", { "name" : fuzzyNumber.name, "color" : "{};{};{}".format(fuzzyNumber.color[0], fuzzyNumber.color[1], fuzzyNumber.color[2])})

        values = np.stack((fuzzyNumber.universe, fuzzyNumber.probability), axis=-1)

        for x, y in values:
            nodeTuple = ET.SubElement(nodeCustom, "tuple")
            nodeX = ET.SubElement(nodeTuple, "x")
            nodeY = ET.SubElement(nodeTuple, "y")
    
            nodeX.text = str(x)
            nodeY.text = str(y)

def loadXML(path):
    """Loads the xml file given in the path and returns two lists. The first containing the loaded numbers and the second containing the loaded ops.

    Keyword arguments:
    path -- the path to the file
    """
    numbers = []
    ops = []

    xmlp = ET.XMLParser(encoding="utf-8")
    tree = ET.parse(path ,parser=xmlp)
    nodeFuzzyoperations = tree.getroot()
    nodeNumbers = nodeFuzzyoperations.find("numbers")
    nodeOps = nodeFuzzyoperations.find("ops")

    if not nodeNumbers == None:
        for element in nodeNumbers:
            numbers.append(loadElement(element))

    if not nodeOps == None:
        for element in nodeOps:
            ops.append(loadElement(element))

    return numbers, ops

def saveXML(path, numbers, ops):
    """Saves the given numbers and ops to an xml file with the specified path.

    Keyword arguments:
    path -- the path to the file
    numbers -- the fuzzy-numbers
    ops -- the operations on the fuzzy-numbers
    """
    nodeFuzzyoperations = ET.Element("fuzzyoperations")
    nodeNumbers = ET.SubElement(nodeFuzzyoperations, "numbers")
    nodeOps = ET.SubElement(nodeFuzzyoperations, "ops")
    
    for number in numbers:
        saveElement(number, [nodeNumbers])

    for op in ops:
        saveElement(op, [nodeOps])

    ET.ElementTree(nodeFuzzyoperations).write(path)