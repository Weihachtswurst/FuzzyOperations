import FuzzyNumber

class Parser:
    def __init__(self, string, isFuzzyFormula, vars={}):
        self.string = string
        self.index = 0
        self.vars = {}
        for var in vars.keys():
            if self.vars.get(var) != None:
                raise Exception("Cannot redefine the value of " + var)
            self.vars[var] = vars[var]
        self.isFuzzyFormula = isFuzzyFormula
    
    def getValue(self):
        value = self.parseExpression()
        self.skipWhitespace()
        if self.hasNext():
            raise Exception(
                "Unexpected character found: '" +
                self.peek() +
                "' at index " +
                str(self.index))
        return value
    
    def peek(self):
        return self.string[self.index:self.index + 1]
    
    def hasNext(self):
        return self.index < len(self.string)
    
    def skipWhitespace(self):
        while self.hasNext():
            if self.peek() in ' \t\n\r':
                self.index += 1
            else:
                return
    
    def parseExpression(self):
        return self.parseAddition()
    
    def parseAddition(self):
        value = self.parseMultiplication()
        while True:
            self.skipWhitespace()
            char = self.peek()
            if char == '+':
                self.index += 1
                value = parserAdd(self.isFuzzyFormula, value, self.parseMultiplication())
            elif char == '-':
                self.index += 1
                value = parserSub(self.isFuzzyFormula, value, self.parseMultiplication())
            else:
                break
        return value
    
    def parseMultiplication(self):
        value = self.parseParenthesis()
        while True:
            self.skipWhitespace()
            char = self.peek()
            if char == '*':
                self.index += 1
                value = parserMult(self.isFuzzyFormula, value, self.parseParenthesis())
            elif char == '/':
                div_index = self.index
                self.index += 1
                denominator = self.parseParenthesis()
                if denominator == 0:
                    raise Exception(
                        "Division by 0 kills baby whales (occured at index " +
                        str(div_index) +
                        ")")
                value = parserDiv(self.isFuzzyFormula, value, denominator)
            else:
                break
        
        return value
    
    def parseParenthesis(self):
        self.skipWhitespace()
        char = self.peek()
        if char == '(':
            self.index += 1
            value = self.parseExpression()
            self.skipWhitespace()
            if self.peek() != ')':
                raise Exception(
                    "No closing parenthesis found at character "
                    + str(self.index))
            value.name = '(' + value.name + ')'
            self.index += 1
            return value
        else:
            return self.parseNegative()
    
    def parseNegative(self):
        self.skipWhitespace()
        char = self.peek()
        if char == '-':
            self.index += 1
            return parserMult(self.isFuzzyFormula, FuzzyNumber.MakeFuzzyNumberFromFloat(-1), self.parseParenthesis())
        else:
            return self.parseValue()
    
    def parseValue(self):
        self.skipWhitespace()
        char = self.peek()
        if char in '0123456789.':
            return self.parseNumber()
        else:
            return self.parseVariable()

    def parseNumber(self):
        self.skipWhitespace()
        strValue = ''
        decimal_found = False
        char = ''
        
        while self.hasNext():
            char = self.peek()            
            if char == '.':
                if decimal_found:
                    raise Exception(
                        "Found an extra period in a number at character " +
                        str(self.index) +
                        ". Are you European?")
                decimal_found = True
                strValue += '.'
            elif char in '0123456789':
                strValue += char
            else:
                break
            self.index += 1
        
        if len(strValue) == 0:
            if char == '':
                raise Exception("Unexpected end found")
            else:
                raise Exception(
                    "I was expecting to find a number at character " +
                    str(self.index) +
                    " but instead I found a '" +
                    char +
                    "'. What's up with that?")
    
        return FuzzyNumber.MakeFuzzyNumberFromFloat(float(strValue))
    
    def parseVariable(self):
        self.skipWhitespace()
        var = ''
        while self.hasNext():
            char = self.peek()
            if char.lower() in '_abcdefghijklmnopqrstuvwxyz0123456789':
                var += char
                self.index += 1
            else:
                break
        
        value = self.vars.get(var, None)
        if value == None:
            raise Exception(
                "Unrecognized variable: '" +
                var +
                "'")
        return value.toLRFuzzyNumber() if not self.isFuzzyFormula else value
    
def calculateExpression(expression, useZadeh, vars={}):
    p = Parser(expression, useZadeh, vars)
    value = p.getValue()
    
    return value

class Verifyer:
    def __init__(self, string, vars={}):
        self.string = string
        self.index = 0
        self.vars = {}
        for var in vars.keys():
            if self.vars.get(var) != None:
                raise Exception("Cannot redefine the value of " + var)
            self.vars[var] = vars[var]
    
    def getValue(self):
        if not self.parseExpression():
            return False
        self.skipWhitespace()
        if self.hasNext():
            return False
        return True
    
    def peek(self):
        return self.string[self.index:self.index + 1]
    
    def hasNext(self):
        return self.index < len(self.string)
    
    def skipWhitespace(self):
        while self.hasNext():
            if self.peek() in ' \t\n\r':
                self.index += 1
            else:
                return
    
    def parseExpression(self):
        return self.parseAddition()
    
    def parseAddition(self):
        if not self.parseMultiplication():
            return False
        while True:
            self.skipWhitespace()
            char = self.peek()
            if char == '+':
                self.index += 1
                if not self.parseMultiplication():
                    return False
            elif char == '-':
                self.index += 1
                if not self.parseMultiplication():
                    return False
            else:
                break
        return True
    
    def parseMultiplication(self):
        if not self.parseParenthesis():
            return False
        while True:
            self.skipWhitespace()
            char = self.peek()
            if char == '*':
                self.index += 1
                if not self.parseParenthesis():
                    return False
            elif char == '/':
                div_index = self.index
                self.index += 1
                #maybe check if denominator == 0
                if not self.parseParenthesis():
                    return False
            else:
                break
        return True
    
    def parseParenthesis(self):
        self.skipWhitespace()
        char = self.peek()
        if char == '(':
            self.index += 1
            if not self.parseExpression():
                return False
            self.skipWhitespace()
            if self.peek() != ')':
                return False
            self.index += 1
            return True
        else:
            return self.parseNegative()
    
    def parseNegative(self):
        self.skipWhitespace()
        char = self.peek()
        if char == '-':
            self.index += 1
            return self.parseParenthesis()
        else:
            return self.parseValue()
    
    def parseValue(self):
        self.skipWhitespace()
        char = self.peek()
        if char in '0123456789.':
            return self.parseNumber()
        else:
            return self.parseVariable()

    def parseNumber(self):
        self.skipWhitespace()
        strValue = ''
        decimal_found = False
        char = ''
        
        while self.hasNext():
            char = self.peek()            
            if char == '.':
                if decimal_found:
                    return False;
                decimal_found = True
                strValue += '.'
            elif char in '0123456789':
                strValue += char
            else:
                break
            self.index += 1
        
        if len(strValue) == 0:
            if char == '':
                return False
            else:
                return False
    
        return True
    
    def parseVariable(self):
        self.skipWhitespace()
        var = ''
        while self.hasNext():
            char = self.peek()
            if char.lower() in '_abcdefghijklmnopqrstuvwxyz0123456789':
                var += char
                self.index += 1
            else:
                break
        
        value = self.vars.get(var, None)
        if value == None:
            return False
        return True
      
def verifyExpression(expression, vars={}):
    p = Verifyer(expression, vars)
    return p.getValue()

def parserAdd(isFuzzyFormula, number1, number2):
    return FuzzyNumber.FuzzyAdd(number1, number2) if isFuzzyFormula else FuzzyNumber.LRAdd(number1, number2)

def parserSub(isFuzzyFormula, number1, number2):
    return FuzzyNumber.FuzzySub(number1, number2) if isFuzzyFormula else FuzzyNumber.LRSub(number1, number2)

def parserMult(isFuzzyFormula, number1, number2):
    return FuzzyNumber.FuzzyMult(number1, number2) if isFuzzyFormula else FuzzyNumber.LRMult(number1, number2)

def parserDiv(isFuzzyFormula, number1, number2):
    return FuzzyNumber.FuzzyDiv(number1, number2) if isFuzzyFormula else FuzzyNumber.LRDiv(number1, number2)