"""@package docstring
CallbackFactory contains a single factory method, that helps inserting arguments into qt-style callbacks.
"""

def CreateCall(func, *args):
    """Returns a function that can be called with no arguments. 
    If this function is called the arguments given in "args" will be inserted.
    
    Keyword arguments:
    func -- the function to call
    args -- the arguments of the function, must match the exact number of arguments that "func" requires
    """
    def callback():
        return func(*args)

    return callback
