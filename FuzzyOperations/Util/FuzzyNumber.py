import skfuzzy as fuzz
from scipy.stats import norm
import numpy as np

class FuzzyNumber(object):
    def __init__(self, name, universe=[], probablity=[], color=(255,0,255)):
        """Constructor for a new FuzzyNumber

        Keyword Arguments:
        name -- Name of the FuzzyNumber
        universe -- The universe of the FuzzyNumber
        probability -- The probability of the FuzzyNumber
        """
        if len(universe) > 1:
            universe, probablity = (list(t) for t in zip(*sorted(zip(universe, probablity))))

        self.name = name
        self.color = color
        self.universe = np.array(universe)
        self.probability = np.array(probablity)
        self.plotted = False

    def convex(self):
        ascending = True
        prev = float('-inf')
        for y in self.probability:
            if ascending:
                if prev > y:
                    ascending = False
            else:
                if prev < y:
                    return False

            prev = y

        return True


    def valid(self):
        for i in range(self.probability.size):
            if self.probability[i] < 0 or self.probability[i] > 1:
                return False

        if self.name != "" and self.universe.size == self.probability.size and self.universe.size > 0 and self.convex():
            return True
        else:
            return False

    def toLRFuzzyNumber(self):
        maxProbability = -1
        firstMaxProbabilityIndex = -1

        for i in range(self.probability.size): # -1 ?
            if self.probability[i] > maxProbability:
                maxProbability = self.probability[i]
                firstMaxProbabilityIndex = i

        maxProbabilityCount = 0
        for i in range(self.probability.size): # -1 ?
            if self.probability[i] == maxProbability:
                maxProbabilityCount += 1

        m = (self.universe[firstMaxProbabilityIndex] + self.universe[firstMaxProbabilityIndex + maxProbabilityCount - 1]) / 2
        alpha = m - self.universe[0]
        beta = self.universe[self.universe.size - 1] - m

        return LRFuzzyNumber(self.name, m, alpha, beta, self.color)

    def __str__(self):
        output = "({}) = {{".format(self.name)
    
        for tuple in np.stack((self.universe, self.probability), axis=-1):
            output += "({}, {}), ".format(tuple[0], tuple[1])

        output = output[:-2] + "}"
        
        return output

    def __unicode__(self):
        return self.__str__

    def __repr__(self):
        return self.__str__

class LRFuzzyNumber(FuzzyNumber):
    def __init__(self, name, m, alpha, beta, color=(255,0,255)):
        """Creates a new LR-Number.

        Keyword arguments:
        name -- The algebraic name of the number (e.G 'A')
        m -- point at 100% probability
        alpha -- The alpha value
        beta -- The beta value
        """
        if alpha > 0 and beta > 0:
            x_axis = np.array([m - alpha, m, m + beta])
            y_axis = np.array([0, 1, 0])
            super().__init__(name, x_axis, y_axis, color)
        elif alpha == 0 and beta > 0:
            x_axis = np.array([m, m + beta])
            y_axis = np.array([1, 0])
            super().__init__(name, x_axis, y_axis, color)
        elif alpha > 0 and beta == 0:
            x_axis = np.array([m - alpha, m])
            y_axis = np.array([0, 1])
            super().__init__(name, x_axis, y_axis, color)
        elif alpha == 0 and beta == 0:
            x_axis = np.array([m])
            y_axis = np.array([1])
            super().__init__(name, x_axis, y_axis, color)
        else:
            super().__init__(name)

        self.m = m
        self.alpha = alpha
        self.beta = beta

    def __str__(self):
        return "({}) = <{}, {}, {}>LR".format(self.name, self.m, self.alpha, self.beta)



def FuzzyAdd(fuzzyNumber1, fuzzyNumber2):
    sum = fuzz.fuzzy_add(fuzzyNumber1.universe, fuzzyNumber1.probability, fuzzyNumber2.universe, fuzzyNumber2.probability)       
    return FuzzyNumber("{} + {}".format(fuzzyNumber1.name, fuzzyNumber2.name), sum[0], sum[1])

def FuzzySub(fuzzyNumber1, fuzzyNumber2):
    sub = fuzz.fuzzymath.fuzzy_sub(fuzzyNumber1.universe, fuzzyNumber1.probability, fuzzyNumber2.universe, fuzzyNumber2.probability)       
    return FuzzyNumber("{} - {}".format(fuzzyNumber1.name, fuzzyNumber2.name), sub[0], sub[1])

def FuzzyMult(fuzzyNumber1, fuzzyNumber2):
    mul = fuzz.fuzzymath.fuzzy_mult(fuzzyNumber1.universe, fuzzyNumber1.probability, fuzzyNumber2.universe, fuzzyNumber2.probability)       
    return FuzzyNumber("{} * {}".format(fuzzyNumber1.name, fuzzyNumber2.name), mul[0], mul[1])

def FuzzyDiv(fuzzyNumber1, fuzzyNumber2):
    div = fuzz.fuzzymath.fuzzy_div(fuzzyNumber1.universe, fuzzyNumber1.probability, fuzzyNumber2.universe, fuzzyNumber2.probability)       
    return FuzzyNumber("{} / {}".format(fuzzyNumber1.name, fuzzyNumber2.name), div[0], div[1])

def LRAdd(LR1, LR2):
    return LRFuzzyNumber("{} + {}".format(LR1.name, LR2.name), LR1.m + LR2.m, LR1.alpha + LR2.alpha, LR1.beta + LR2.beta)

def LRSub(LR1, LR2):
    return LRFuzzyNumber("{} - {}".format(LR1.name, LR2.name), LR1.m - LR2.m, LR1.alpha + LR2.beta, LR1.beta + LR2.alpha)

def LRMult(LR1, LR2):
    return LRFuzzyNumber("{} * {}".format(LR1.name, LR2.name), LR1.m * LR2.m, LR1.m * LR2.alpha + LR2.m * LR1.alpha, LR1.m * LR2.beta + LR2.m * LR1.beta)

def LRDiv(LR1, LR2):
    return LRFuzzyNumber("{} / {}".format(LR1.name, LR2.name), LR1.m / LR2.m, (LR1.m * LR2.beta + LR2.m * LR1.alpha) / (LR2.m * LR2.m), (LR1.m * LR2.alpha + LR2.m * LR1.beta) / ((LR2.m * LR2.m)))

def MakeFuzzyNumberFromFloat(floatValue):
    return LRFuzzyNumber(str(floatValue), floatValue, 0, 0, (125,125,125))