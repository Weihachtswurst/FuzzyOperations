"""@package docstring
Entry point for the FuzzyOperations Application.
"""

import sys
import os

if __name__ == "__main__":
    # Honor windows scaling settings.  Does not seem to work atm.
    os.environ["QT_AUTO_SCREEN_SCALE_FACTOR"] = "1"
    
    # We cannot guarantee that our applet works with older versions.
    if sys.version_info[0] < 3 or sys.version_info[1] < 6:
        print("Python 3.6 recommended")

    # Print some infos about the environment
    from PyQt5.QtCore import QT_VERSION_STR
    from PyQt5.Qt import PYQT_VERSION_STR
    from sip import SIP_VERSION_STR
    print("Python version: {}.{}".format(sys.version_info[0], sys.version_info[1]))
    print("Qt version:", QT_VERSION_STR)
    print("SIP version:", SIP_VERSION_STR)
    print("PyQt version:", PYQT_VERSION_STR)
    print("\n")

    # VS Intellisense sucks and can't deal with subfolders/packages in user projects
    # To avoid error messages in the IDE we have to add every internal package path here
    curPath = sys.argv[0][:sys.argv[0].rfind("\\") + 1]
    sys.path.insert(0, curPath + "GUI")
    sys.path.insert(0, curPath + "Parser")
    sys.path.insert(0, curPath + "Util")

    # Set anti-aliasing o the graphs to True
    import pyqtgraph as pg
    pg.setConfigOptions(antialias=True)

    # We want to compile the latest qt design files to python code in debuug
    if __debug__:
        from subprocess import Popen
        from os import name

        print("Running in Debug Mode")

        if name != 'nt':
            print("Detected OS that is not Windows.\nCompiling Qt-Resources to Python not avaiable.")
        else:
            print("Trying to compile Qt-Resources to Python")

            try:
                Popen("CompileQt.bat").communicate()
                print("\nQt-Resources successfully compiled")
            except:
                print("\nCannot execute CompileQt.bat")

    print("Starting Application")

    # Import here, because the MainWindowBase may have just changed
    from MainWindow import Start
    Start()
