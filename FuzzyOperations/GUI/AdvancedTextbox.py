from PyQt5 import QtWidgets, QtCore

class AdvancedLineEdit(QtWidgets.QLineEdit):
    keyPressed = QtCore.pyqtSignal(int)

    def keyPressEvent(self, event):
        super(AdvancedLineEdit, self).keyPressEvent(event)
        self.keyPressed.emit(event.key())
