from LRDialogBase import Ui_LRDialogBase
from FuzzyNumber import LRFuzzyNumber
from MainWindow import widMain
from PyQt5 import QtGui, QtCore, QtWidgets

class LRDialog(Ui_LRDialogBase):
    def __init__(self):
        self.confirmed = False
        self.name = ""
        self.color = (255, 0, 255)
        self.m = 0
        self.alpha = 0
        self.beta = 0

        self.dialog = QtGui.QDialog(None, QtCore.Qt.WindowSystemMenuHint | QtCore.Qt.WindowTitleHint | QtCore.Qt.WindowCloseButtonHint)
        self.setupUi(self.dialog)

    def setupUi(self, MainWindow):
        super().setupUi(MainWindow)

        self.txtName.setText(self.name)
        self.txtM.setText(str(self.m))
        self.txtAlpha.setText(str(self.alpha))
        self.txtBeta.setText(str(self.beta))
        
        self.txtName.textChanged.connect(self.textNameChanged)
        self.txtM.textChanged.connect(self.textMChanged)
        self.txtAlpha.textChanged.connect(self.textAlphaChanged)
        self.txtBeta.textChanged.connect(self.textBetaChanged)

        self.btnCancel.clicked.connect(self.cancel)
        self.btnAdd.clicked.connect(self.confirm)
        self.btnColor.clicked.connect(self.changeColor)

    def show(self):
        self.confirmed = False
        self.name = ""
        self.color = (255, 0, 255)
        self.m = 0
        self.alpha = 0
        self.beta = 0

        self.txtName.setText(self.name)
        self.txtM.setText(str(self.m))
        self.txtAlpha.setText(str(self.alpha))
        self.txtBeta.setText(str(self.beta))
        self.btnColor.setStyleSheet("background: rgb({}, {}, {});".format(self.color[0], self.color[1], self.color[2]))

        self.plotAndValidate()
        self.dialog.exec_()

        if self.confirmed:
            return LRFuzzyNumber(self.name, self.m, self.alpha, self.beta, self.color)
        else:
            return None

    def cancel(self):
        self.dialog.close()

    def confirm(self):
        self.confirmed = True
        self.dialog.close()

    def changeColor(self):
        color = QtWidgets.QColorDialog.getColor()

        if color.isValid():
            self.color = (color.red(), color.green(), color.blue())
            self.btnColor.setStyleSheet("background: rgb({}, {}, {});".format(self.color[0], self.color[1], self.color[2]))

            self.plotAndValidate()

    def plotAndValidate(self):
        fuzzy = LRFuzzyNumber(self.name, self.m, self.alpha, self.beta, self.color)

        self.pltLR.clear()
        plot = self.pltLR.plot()
        plot.setPen(color=fuzzy.color, width=widMain[0].lineThickness)
        plot.setData(fuzzy.universe, fuzzy.probability)

        if fuzzy.valid():
            self.btnAdd.setEnabled(True)
        else:
            self.btnAdd.setEnabled(False)

    def textMChanged(self, text):
        try:
            self.m = float(text.replace(" ", ""))
            self.plotAndValidate()
        except:
            pass

    def textAlphaChanged(self, text):
        try:
            self.alpha = float(text.replace(" ", ""))
            self.plotAndValidate()
        except:
            pass

    def textBetaChanged(self, text):
        try:
            self.beta = float(text.replace(" ", ""))
            self.plotAndValidate()
        except:
            pass

    def textNameChanged(self, text):
        if self.name != text:
            text = text.replace(" ", "")
            if len(text) > 0:
                if text[0].isdigit():
                    text = "_" + text
            self.name = text
            self.txtName.setText(text)

            self.plotAndValidate()
