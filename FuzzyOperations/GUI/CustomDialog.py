from CustomDialogBase import Ui_CustomDialogBase
from FuzzyNumber import FuzzyNumber
from MainWindow import widMain
from PyQt5 import QtGui, QtCore, QtWidgets

class CustomDialog(Ui_CustomDialogBase):
    def __init__(self):
        self.confirmed = False
        self.addingRow = False
        self.name = ""
        self.color = (255, 0, 255)

        self.dialog = QtGui.QDialog(None, QtCore.Qt.WindowSystemMenuHint | QtCore.Qt.WindowTitleHint | QtCore.Qt.WindowCloseButtonHint)
        self.setupUi(self.dialog)

    def setupUi(self, MainWindow):
        super().setupUi(MainWindow)

        self.txtName.setText(self.name)
        self.txtName.textChanged.connect(self.textNameChanged)

        self.btnCancel.clicked.connect(self.cancel)
        self.btnAdd.clicked.connect(self.confirm)
        self.btnColor.clicked.connect(self.changeColor)

        self.addRow()
        self.tblCustom.setHorizontalHeaderLabels(["x", "μ(x)"])
        self.tblCustom.itemChanged.connect(self.tableChanged)

    def tableKeyEvent(self):
        pass

    def tableChanged(self):
        if not self.addingRow:
            self.plotAndValidate()

            firstColumn = self.tblCustom.item(self.tblCustom.rowCount() - 1, 0).text()
            secondColumn = self.tblCustom.item(self.tblCustom.rowCount() - 1, 1).text()

            if firstColumn != "" or secondColumn != "":
                self.addRow()

    def addRow(self):
        self.addingRow = True

        self.tblCustom.insertRow(self.tblCustom.rowCount())
        self.tblCustom.setItem(self.tblCustom.rowCount() - 1, 0, QtGui.QTableWidgetItem())
        self.tblCustom.setItem(self.tblCustom.rowCount() - 1, 1, QtGui.QTableWidgetItem())
        self.tblCustom.item(self.tblCustom.rowCount() - 1, 0).setText("")
        self.tblCustom.item(self.tblCustom.rowCount() - 1, 1).setText("")

        self.addingRow = False

    def show(self):
        self.confirmed = False
        self.addingRow = False
        self.name = ""
        self.color = (255, 0, 255)

        self.btnColor.setStyleSheet("background: rgb({}, {}, {});".format(self.color[0], self.color[1], self.color[2]))
        self.txtName.setText(self.name)
        self.tblCustom.setRowCount(0)
        self.addRow()

        self.plotAndValidate()
        self.dialog.exec_()

        if self.confirmed:
            universe, probability = self.getDataOfTable()

            return FuzzyNumber(self.name, universe, probability, self.color)
        else:
            return None

    def cancel(self):
        self.dialog.close()

    def confirm(self):
        self.confirmed = True
        self.dialog.close()

    def getDataOfTable(self):
        universe = []
        probability = []

        for row in range(self.tblCustom.rowCount()):
            x = self.tblCustom.item(row, 0).text()
            y = self.tblCustom.item(row, 1).text()

            try:
                x = float(x)
                y = float(y)
            except:
                continue

            universe.append(x)
            probability.append(y)

        return universe, probability

    def changeColor(self):
        color = QtWidgets.QColorDialog.getColor()

        if color.isValid():
            self.color = (color.red(), color.green(), color.blue())
            self.btnColor.setStyleSheet("background: rgb({}, {}, {});".format(self.color[0], self.color[1], self.color[2]))

            self.plotAndValidate()

    def plotAndValidate(self):
        universe, probability = self.getDataOfTable()

        fuzzy = FuzzyNumber(self.name, universe, probability, self.color)

        self.pltCustom.clear()
        plot = self.pltCustom.plot()
        plot.setPen(color=fuzzy.color, width=widMain[0].lineThickness)
        plot.setData(fuzzy.universe, fuzzy.probability)

        if fuzzy.valid():
            self.btnAdd.setEnabled(True)
        else:
            self.btnAdd.setEnabled(False)

    def textNameChanged(self, text):
        if self.name != text:
            text = text.replace(" ", "")
            if len(text) > 0:
                if text[0].isdigit():
                    text = "_" + text
            self.name = text
            self.txtName.setText(text)

            self.plotAndValidate()
