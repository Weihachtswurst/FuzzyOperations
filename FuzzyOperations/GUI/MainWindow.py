"""@package docstring
The MainWindow for the FuzzyOperations Application.
"""

widMain = [None]
app = None


import sys
import traceback
import XMLParser
from EquationParser import verifyExpression
from PyQt5 import QtWidgets, QtGui, QtCore
from MainWindowBase import Ui_MainWindow
from AddSetDialog import AddSetDialog, AddOpDialog
from AdvancedTextbox import AdvancedLineEdit
from ItemWidget import ItemWidget
from FuzzyNumber import *
from CallbackFactory import CreateCall


class MainWindow(Ui_MainWindow):
    def __init__(self, main):
        """Default Constructor for the MainWindow"""
        self.fontActionGroup = QtWidgets.QActionGroup(main)   
        self.lineThicknessGroup = QtWidgets.QActionGroup(main)

        super().__init__()

        self.livePlot = False
        self.defaultFontSize = -1
        self.lineThickness = 1

        self.addSetDialog = AddSetDialog()
        self.addOpDialog = AddOpDialog()
        self.main = main

        self.setItemWidgets = []
        self.opItemWidgets = []

        self.setPlots = []
        self.opPlots = []
        self.setFuzzy = []
        self.opFuzzy = []

        global widMain
        widMain[0] = self

    def setupUi(self, MainWindow):
        global font
        
        """Builds the UI"""
        super().setupUi(MainWindow)

        self.btnAddSet.clicked.connect(self.addSetClicked)
        self.btnAddOp.clicked.connect(self.addOpClicked)
        self.btnPlot.clicked.connect(CreateCall(self.plotOps, False))
        self.actionLoad.triggered.connect(self.loadFile)
        self.actionSave.triggered.connect(self.saveFile)
        self.actionFontDefault.triggered.connect(CreateCall(self.changeFontSize, 'Default'))
        self.actionFontSize10.triggered.connect(CreateCall(self.changeFontSize, 10))
        self.actionFontSize12.triggered.connect(CreateCall(self.changeFontSize, 12))
        self.actionFontSize14.triggered.connect(CreateCall(self.changeFontSize, 14))
        self.actionFontSize18.triggered.connect(CreateCall(self.changeFontSize, 18))
        self.actionFontSize22.triggered.connect(CreateCall(self.changeFontSize, 22))
        self.actionFontSize28.triggered.connect(CreateCall(self.changeFontSize, 28))
        self.actionThickness1.triggered.connect(CreateCall(self.changeLineThickness, 1))
        self.actionThickness2.triggered.connect(CreateCall(self.changeLineThickness, 2))
        self.actionThickness3.triggered.connect(CreateCall(self.changeLineThickness, 3))
        self.actionThickness5.triggered.connect(CreateCall(self.changeLineThickness, 5))
        self.fontActionGroup.addAction(self.actionFontDefault)
        self.fontActionGroup.addAction(self.actionFontSize10)
        self.fontActionGroup.addAction(self.actionFontSize12)
        self.fontActionGroup.addAction(self.actionFontSize14)
        self.fontActionGroup.addAction(self.actionFontSize18)
        self.fontActionGroup.addAction(self.actionFontSize22)
        self.fontActionGroup.addAction(self.actionFontSize28)
        self.fontActionGroup.setExclusive(True)
        self.lineThicknessGroup.addAction(self.actionThickness1)
        self.lineThicknessGroup.addAction(self.actionThickness2)
        self.lineThicknessGroup.addAction(self.actionThickness3)
        self.lineThicknessGroup.addAction(self.actionThickness5)
        self.lineThicknessGroup.setExclusive(True)

        self.chkLivePlot.stateChanged.connect(self.switchLivePlot)

    def changeFontSize(self, size):
        if self.defaultFontSize == -1 and size == 'Default':
            return

        font = app.font()

        if self.defaultFontSize == -1:
            self.defaultFontSize = font.pointSize()

        if size == 'Default':
            size = self.defaultFontSize

        font.setPointSize(size)
        app.setFont(font)

    def changeLineThickness(self, size):
        self.lineThickness = size

        self.plotSets()
        self.plotOps(True)

    def switchLivePlot(self):
        """Method to handle selection of the LivePlotting-Checkbox"""
        if self.chkLivePlot.isChecked():
            self.btnPlot.setEnabled(False)
            self.livePlot = True
            self.plotOps(False)
        else:
            self.btnPlot.setEnabled(True)
            self.livePlot = False

    def loadFile(self):
        """Opens a file dialog where a user can select a save. The save's contents will get loaded into the current instance"""
        file = QtGui.QFileDialog.getOpenFileName(self.main, 'Datei speichern', 'c:\\',"*.fop *.xml")         
        
        if file[0] != '':
            sets, ops = XMLParser.loadXML(file[0])

            while len(self.setItemWidgets) != 0:
                self.setItemWidgets[0].btnRemove.click()

            while len(self.opItemWidgets) != 0:
                self.opItemWidgets[0].btnRemove.click()

            for set in sets:
                if set != None:
                    self.addSet(set)

            for op in ops:
                if op != None:
                    self.addOp(op)

            self.plotSets()
            if self.livePlot:
                self.plotOps(False)

    def saveFile(self):
        """Opens a file dialog to specify a file where the current numbers and ops should be saved."""
        file = QtGui.QFileDialog.getSaveFileName(self.main, 'Datei laden', 'c:\\',"*.fop *.xml")        
        
        if file[0] != '':
            XMLParser.saveXML(file[0], self.setFuzzy, self.opFuzzy)

    def plotSets(self):
        """Plots all Sets"""
        for plot in self.setPlots:
            plot.clear()

        del self.setPlots[:]

        for set in self.setFuzzy:
            plot = self.graphicsView.plot()
            plot.setPen(color=set.color, width=self.lineThickness)
            plot.setData(set.universe, set.probability)            
            set.plotted = True

            self.setPlots.append(plot)

    def plotOps(self, plotOnlyAlreadyPlottedOps):
        """Plots all Operations"""
        for plot in self.opPlots:
            plot.clear()
        
        del self.opPlots[:]
        
        for op in self.opFuzzy:
            if plotOnlyAlreadyPlottedOps and op.plotted == False:
                continue

            plot = self.graphicsView.plot()
            plot.setPen(color=op.color, width=self.lineThickness)
            plot.setData(op.universe, op.probability)            
            op.plotted = True

            self.opPlots.append(plot)

    def changeColorClicked(self, fuzzy, itemWidget):
        color = QtWidgets.QColorDialog.getColor()

        if color.isValid():
            fuzzy.color = (color.red(), color.green(), color.blue())
            itemWidget.btnColor.setStyleSheet("background: rgb({}, {}, {});".format(color.red(), color.green(), color.blue()))

            self.plotSets()
            self.plotOps(True)

    def addSetClicked(self):
        """Method to handle a click on the button for adding a new set."""
        newSet = self.addSetDialog.show()

        if not newSet == None:
            self.addSet(newSet)
    
    def addSet(self, newSet):
        self.setFuzzy.append(newSet)
            
        newItem = ItemWidget(self.frmSets, newSet, self.removeSetClicked, self.changeColorClicked)
        self.setItemWidgets.append(newItem)

        self.lytSets.addWidget(newItem.widget)
        self.lytSets.removeWidget(self.btnAddSet)
        self.lytSets.addWidget(self.btnAddSet)
        
        self.plotSets()

    def removeSetClicked(self, set, itemWidget):
        self.setFuzzy.remove(set)
        self.setItemWidgets.remove(itemWidget)

        self.lytSets.removeWidget(itemWidget.widget)

        dictionary = {}
        for tmpSet in self.setFuzzy:
            dictionary[tmpSet.name] = tmpSet       
            
        subtractingValue = 0
        for i in range(len(self.opFuzzy)):
            if not verifyExpression(self.opFuzzy[i - subtractingValue].name, dictionary):
                self.opItemWidgets[i - subtractingValue].btnRemove.click()    
                subtractingValue += 1

        self.graphicsView.clear()
        self.plotSets()
        self.plotOps(True)

        itemWidget.widget.setParent(None)

    def addOpClicked(self):
        """Method to handle a click on the button for adding a new operation."""
        newOp = self.addOpDialog.show()

        if not newOp == None:
            self.addOp(newOp)

    def addOp(self, newOp):
        """Method to handle a click on the button for adding a new operation."""
        self.opFuzzy.append(newOp)
            
        newItem = ItemWidget(self.frmOps, newOp, self.removeOpClicked, self.changeColorClicked)
        self.opItemWidgets.append(newItem)

        self.lytOps.addWidget(newItem.widget)
        self.lytOps.removeWidget(self.btnAddOp)
        self.lytOps.addWidget(self.btnAddOp)
        
        if self.livePlot:
            self.plotOps(False)

    def removeOpClicked(self, op, itemWidget):
        #plotOps removes op from opFuzzy, by calling calculateFormulas
        self.opFuzzy.remove(op)
        self.opItemWidgets.remove(itemWidget)

        self.graphicsView.clear()
        self.plotSets()
        self.plotOps(True)

        itemWidget.widget.setParent(None)

def Start():
    global app

    """Entry Point for the MainWindow, based on Qt"""
    QtWidgets.QApplication.setAttribute(QtCore.Qt.AA_DisableHighDpiScaling)
    app = QtWidgets.QApplication(sys.argv)
    app.setAttribute(QtCore.Qt.AA_DisableHighDpiScaling)

    #app.setFont(QtGui.QFont("Times", 20, QtGui.QFont.Bold) )
    widMain = QtWidgets.QMainWindow()
    
    ui = MainWindow(widMain)
    ui.setupUi(widMain)
    widMain.show()

    sys.exit(app.exec_())


