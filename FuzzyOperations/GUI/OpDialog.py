from EquationParser import *
import MainWindow
from OpDialogBase import Ui_OpDialogBase
from PyQt5 import QtGui, QtCore, QtWidgets

class OpDialog(Ui_OpDialogBase):
    def __init__(self):
        self.confirmed = False
        self.useZadeh = False
        self.color = (255, 0, 255)
        self.dialog = QtGui.QDialog(None, QtCore.Qt.WindowSystemMenuHint | QtCore.Qt.WindowTitleHint | QtCore.Qt.WindowCloseButtonHint)
        
        self.setupUi(self.dialog)

    def setupUi(self, MainWindow):
        super().setupUi(MainWindow)

        self.txtFormula.textChanged.connect(self.textFormulaChanged)
        self.txtFormula.setText("")
        self.btnAdd.clicked.connect(self.confirm)
        self.btnCancel.clicked.connect(self.cancel)
        self.btnColor.clicked.connect(self.changeColor)

    def show(self, useZadeh):
        self.confirmed = False
        self.valid = False
        self.useZadeh = useZadeh
        self.color = (255, 0, 255)

        self.btnColor.setStyleSheet("background: rgb({}, {}, {});".format(self.color[0], self.color[1], self.color[2]))
        self.txtFormula.setText("")

        self.dialog.exec_()

        if self.confirmed:
            dictionary = {}

            for set in MainWindow.widMain[0].setFuzzy:
                dictionary[set.name] = set       

            number = calculateExpression(self.txtFormula.text(), self.useZadeh, dictionary)
            number.color = self.color
            
            return number
        else:
            return None

    def cancel(self):
        self.dialog.close()

    def confirm(self):
        self.confirmed = True
        self.dialog.close()

    def changeColor(self):
        color = QtWidgets.QColorDialog.getColor()

        if color.isValid():
            self.color = (color.red(), color.green(), color.blue())
            self.btnColor.setStyleSheet("background: rgb({}, {}, {});".format(self.color[0], self.color[1], self.color[2]))

    def textFormulaChanged(self):
        dictionary = {}

        for set in MainWindow.widMain[0].setFuzzy:
            dictionary[set.name] = set       

        self.valid = verifyExpression(self.txtFormula.text(), dictionary)
        #TODO Color coding here

    @property
    def valid(self):
        """I'm the 'x' property."""
        return self._valid

    @valid.setter
    def valid(self, value):
        self._valid = value

        self.btnAdd.setEnabled(self._valid)

                #newTextbox = AdvancedLineEdit(self.frmOps)
        #newTextbox.keyPressed.connect(self.opOnKey)
        #newTextbox.setStyleSheet("border: 2px solid red;")