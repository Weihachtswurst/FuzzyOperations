# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\Users\Anwender\Source\Repos\FuzzyOperations\FuzzyOperations\GUI\AddSetDialogBase.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(400, 300)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/icon/icon.ico"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        Dialog.setWindowIcon(icon)
        self.verticalLayout = QtWidgets.QVBoxLayout(Dialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.wdgCentral = QtWidgets.QWidget(Dialog)
        self.wdgCentral.setObjectName("wdgCentral")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.wdgCentral)
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_2.setSpacing(6)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.lstCentral = QtWidgets.QListWidget(self.wdgCentral)
        self.lstCentral.setObjectName("lstCentral")
        self.verticalLayout_2.addWidget(self.lstCentral)
        self.verticalLayout.addWidget(self.wdgCentral)
        self.wdgSouth = QtWidgets.QWidget(Dialog)
        self.wdgSouth.setObjectName("wdgSouth")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.wdgSouth)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setSpacing(6)
        self.horizontalLayout.setObjectName("horizontalLayout")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.btnCanel = QtWidgets.QPushButton(self.wdgSouth)
        self.btnCanel.setObjectName("btnCanel")
        self.horizontalLayout.addWidget(self.btnCanel)
        self.btnConfirm = QtWidgets.QPushButton(self.wdgSouth)
        self.btnConfirm.setDefault(True)
        self.btnConfirm.setObjectName("btnConfirm")
        self.horizontalLayout.addWidget(self.btnConfirm)
        self.verticalLayout.addWidget(self.wdgSouth)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        self.btnCanel.setText(_translate("Dialog", "Abbrechen"))
        self.btnConfirm.setText(_translate("Dialog", "Hinzufügen"))

import Resource_rc
