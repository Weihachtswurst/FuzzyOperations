from ItemWidgetBase import Ui_ItemWidgetBase
from CallbackFactory import CreateCall
from PyQt5 import QtGui
import FuzzyNumber

class ItemWidget(Ui_ItemWidgetBase):
    def __init__(self, parent, fuzzy, onRemoveClicked, onColorClicked):
        self.widget = QtGui.QWidget(parent)
        self.setupUi(self.widget)

        self.txtFormula.setText(str(fuzzy))
        self.btnColor.setStyleSheet("background: rgb({}, {}, {});".format(fuzzy.color[0], fuzzy.color[1], fuzzy.color[2]))
        self.btnColor.clicked.connect(CreateCall(onColorClicked, fuzzy, self))
        self.btnRemove.clicked.connect(CreateCall(onRemoveClicked, fuzzy, self))

        