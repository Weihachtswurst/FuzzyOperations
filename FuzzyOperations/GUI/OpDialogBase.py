# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\Users\Anwender\Source\Repos\FuzzyOperations\FuzzyOperations\GUI\OpDialogBase.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_OpDialogBase(object):
    def setupUi(self, OpDialogBase):
        OpDialogBase.setObjectName("OpDialogBase")
        OpDialogBase.resize(400, 67)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/icon/icon.ico"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        OpDialogBase.setWindowIcon(icon)
        self.verticalLayout = QtWidgets.QVBoxLayout(OpDialogBase)
        self.verticalLayout.setObjectName("verticalLayout")
        self.wdgNorth = QtWidgets.QWidget(OpDialogBase)
        self.wdgNorth.setObjectName("wdgNorth")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self.wdgNorth)
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.lblFormula = QtWidgets.QLabel(self.wdgNorth)
        self.lblFormula.setObjectName("lblFormula")
        self.horizontalLayout_2.addWidget(self.lblFormula)
        self.txtFormula = QtWidgets.QLineEdit(self.wdgNorth)
        self.txtFormula.setObjectName("txtFormula")
        self.horizontalLayout_2.addWidget(self.txtFormula)
        self.btnColor = QtWidgets.QToolButton(self.wdgNorth)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.btnColor.sizePolicy().hasHeightForWidth())
        self.btnColor.setSizePolicy(sizePolicy)
        self.btnColor.setMaximumSize(QtCore.QSize(20, 20))
        self.btnColor.setStyleSheet("background: rgb(255, 0, 255)")
        self.btnColor.setText("")
        self.btnColor.setAutoRaise(True)
        self.btnColor.setObjectName("btnColor")
        self.horizontalLayout_2.addWidget(self.btnColor)
        self.txtFormula.raise_()
        self.lblFormula.raise_()
        self.btnColor.raise_()
        self.verticalLayout.addWidget(self.wdgNorth)
        self.wdgSouth = QtWidgets.QWidget(OpDialogBase)
        self.wdgSouth.setObjectName("wdgSouth")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.wdgSouth)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        spacerItem = QtWidgets.QSpacerItem(217, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.btnCancel = QtWidgets.QPushButton(self.wdgSouth)
        self.btnCancel.setAutoDefault(True)
        self.btnCancel.setObjectName("btnCancel")
        self.horizontalLayout.addWidget(self.btnCancel)
        self.btnAdd = QtWidgets.QPushButton(self.wdgSouth)
        self.btnAdd.setAutoDefault(True)
        self.btnAdd.setDefault(True)
        self.btnAdd.setObjectName("btnAdd")
        self.horizontalLayout.addWidget(self.btnAdd)
        self.verticalLayout.addWidget(self.wdgSouth)

        self.retranslateUi(OpDialogBase)
        QtCore.QMetaObject.connectSlotsByName(OpDialogBase)

    def retranslateUi(self, OpDialogBase):
        _translate = QtCore.QCoreApplication.translate
        OpDialogBase.setWindowTitle(_translate("OpDialogBase", "Operation hinzufügen"))
        self.lblFormula.setText(_translate("OpDialogBase", "<html><head/><body><p align=\"right\"><span style=\" font-size:10pt; font-weight:600;\">Gleichung:</span></p></body></html>"))
        self.btnCancel.setText(_translate("OpDialogBase", "Abbrechen"))
        self.btnAdd.setText(_translate("OpDialogBase", "Hinzufügen"))

import Resource_rc
