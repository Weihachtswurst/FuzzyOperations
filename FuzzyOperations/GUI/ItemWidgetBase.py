# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\Users\Anwender\Source\Repos\FuzzyOperations\FuzzyOperations\GUI\ItemWidgetBase.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_ItemWidgetBase(object):
    def setupUi(self, ItemWidgetBase):
        ItemWidgetBase.setObjectName("ItemWidgetBase")
        ItemWidgetBase.resize(380, 38)
        self.horizontalLayout = QtWidgets.QHBoxLayout(ItemWidgetBase)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setSpacing(6)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.btnRemove = QtWidgets.QToolButton(ItemWidgetBase)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.btnRemove.sizePolicy().hasHeightForWidth())
        self.btnRemove.setSizePolicy(sizePolicy)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/icon/remove.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btnRemove.setIcon(icon)
        self.btnRemove.setAutoRaise(False)
        self.btnRemove.setObjectName("btnRemove")
        self.horizontalLayout.addWidget(self.btnRemove)
        self.txtFormula = QtWidgets.QLineEdit(ItemWidgetBase)
        self.txtFormula.setObjectName("txtFormula")
        self.horizontalLayout.addWidget(self.txtFormula)
        self.btnColor = QtWidgets.QToolButton(ItemWidgetBase)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.btnColor.sizePolicy().hasHeightForWidth())
        self.btnColor.setSizePolicy(sizePolicy)
        self.btnColor.setMaximumSize(QtCore.QSize(20, 20))
        self.btnColor.setStyleSheet("background: rgb(255, 0, 255)")
        self.btnColor.setText("")
        self.btnColor.setAutoRaise(True)
        self.btnColor.setObjectName("btnColor")
        self.horizontalLayout.addWidget(self.btnColor)

        self.retranslateUi(ItemWidgetBase)
        QtCore.QMetaObject.connectSlotsByName(ItemWidgetBase)

    def retranslateUi(self, ItemWidgetBase):
        _translate = QtCore.QCoreApplication.translate
        ItemWidgetBase.setWindowTitle(_translate("ItemWidgetBase", "Form"))
        self.btnRemove.setText(_translate("ItemWidgetBase", "..."))

import Resource_rc
