from AddSetDialogBase import Ui_Dialog
from CustomDialog import CustomDialog
from LRDialog import LRDialog
from OpDialog import OpDialog
from CallbackFactory import CreateCall
from PyQt5 import QtGui, QtCore


class AddSetDialogBaseEx(Ui_Dialog):
    def __init__(self):
        self.itemHeight = 50
        self.newSet = None
        self.dialog = QtGui.QDialog(None, QtCore.Qt.WindowSystemMenuHint | QtCore.Qt.WindowTitleHint | QtCore.Qt.WindowCloseButtonHint)
        
        self.setupUi(self.dialog)

    def setupUi(self, MainWindow):
        """Builds the UI"""
        super().setupUi(MainWindow)

        self.lstCentral.setIconSize(QtCore.QSize(self.itemHeight, self.itemHeight))
        self.lstCentral.itemDoubleClicked.connect(self.listDoubleClicked)
        self.btnCanel.clicked.connect(self.cancelClicked)
        self.btnConfirm.clicked.connect(self.confirmClicked)    

        for key in self.itemDialogs:
            icon = QtGui.QIcon()
            icon.addPixmap(QtGui.QPixmap(self.itemIconNames[key]), QtGui.QIcon.Normal, QtGui.QIcon.Off)
            item = QtGui.QListWidgetItem()
            item.setSizeHint(QtCore.QSize(-1, self.itemHeight))
            item.setIcon(icon)
            item.setText(key)

            self.lstCentral.addItem(item)

        self.lstCentral.setCurrentRow(0)

    def cancelClicked(self):
        """Cancel adding a new set and close the dialog."""
        self.dialog.close()

    def listDoubleClicked(self, item):
        """Method to handle a double click on one of the central list items."""
        self.openNextDialog(item.text())

    def confirmClicked(self):
        """Method to handle a click on the confirm button."""
        self.openNextDialog(self.lstCentral.currentItem().text())

    def openNextDialog(self, name):
        """Opens the next dialog to choose the exact FuzzyNumber based on the given name.

        Keyword arguments:
        name -- The name of the choosen type of FuzzyNumber
        """
        self.newSet = self.itemDialogs[name]()
        self.dialog.close()

    def show(self):
        """Opens this dialog and returns a new FuzzyNumber or None."""
        self.newSet = None

        self.dialog.exec_()

        return self.newSet

class AddSetDialog(AddSetDialogBaseEx):
    def __init__(self):
        """Default Constructor for the AddSetDialog"""
        self.customDialog = CustomDialog()
        self.lrDialog = LRDialog()
        
        self.itemDialogs = {"Benutzerdefiniert" : CreateCall(self.customDialog.show), "LR-Zahl" : CreateCall(self.lrDialog.show)}
        self.itemIconNames = {"Benutzerdefiniert" : ":/preview/custom.png", "LR-Zahl" : ":/preview/lr.png"}
        
        super().__init__()

    def setupUi(self, Dialog):
        """Builds the UI"""
        super().setupUi(Dialog)

        Dialog.setWindowTitle("Fuzzy-Zahl hinzufügen")

class AddOpDialog(AddSetDialogBaseEx):
    def __init__(self):
        """Default Constructor for the AddSetDialog"""
        self.opDialogZadeh = OpDialog()
        self.opDialogLR = OpDialog()

        self.itemDialogs = {"Erweiterungsprinzip nach Zadeh" : CreateCall(self.opDialogZadeh.show, True), "Näherung durch LR-Zahlen" : CreateCall(self.opDialogLR.show, False)}
        self.itemIconNames = {"Erweiterungsprinzip nach Zadeh" : ":/preview/zadeh.png", "Näherung durch LR-Zahlen" : ":/preview/lr_ext.png"}

        super().__init__()

    def setupUi(self, Dialog):
        """Builds the UI"""
        super().setupUi(Dialog)

        Dialog.setWindowTitle("Operation hinzufügen")

import Resource_rc