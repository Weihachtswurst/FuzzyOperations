# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\Users\Anwender\Source\Repos\FuzzyOperations\FuzzyOperations\GUI\CustomDialogBase.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_CustomDialogBase(object):
    def setupUi(self, CustomDialogBase):
        CustomDialogBase.setObjectName("CustomDialogBase")
        CustomDialogBase.resize(400, 300)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/icon/icon.ico"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        CustomDialogBase.setWindowIcon(icon)
        self.verticalLayout = QtWidgets.QVBoxLayout(CustomDialogBase)
        self.verticalLayout.setObjectName("verticalLayout")
        self.wdgNorth = QtWidgets.QWidget(CustomDialogBase)
        self.wdgNorth.setObjectName("wdgNorth")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self.wdgNorth)
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.lblName = QtWidgets.QLabel(self.wdgNorth)
        self.lblName.setObjectName("lblName")
        self.horizontalLayout_2.addWidget(self.lblName)
        self.txtName = QtWidgets.QLineEdit(self.wdgNorth)
        self.txtName.setObjectName("txtName")
        self.horizontalLayout_2.addWidget(self.txtName)
        self.btnColor = QtWidgets.QToolButton(self.wdgNorth)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.btnColor.sizePolicy().hasHeightForWidth())
        self.btnColor.setSizePolicy(sizePolicy)
        self.btnColor.setMaximumSize(QtCore.QSize(20, 20))
        self.btnColor.setStyleSheet("background: rgb(255, 0, 255)")
        self.btnColor.setText("")
        self.btnColor.setAutoRaise(True)
        self.btnColor.setObjectName("btnColor")
        self.horizontalLayout_2.addWidget(self.btnColor)
        self.verticalLayout.addWidget(self.wdgNorth)
        self.tblCustom = QtWidgets.QTableWidget(CustomDialogBase)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.tblCustom.sizePolicy().hasHeightForWidth())
        self.tblCustom.setSizePolicy(sizePolicy)
        self.tblCustom.setContextMenuPolicy(QtCore.Qt.NoContextMenu)
        self.tblCustom.setMidLineWidth(0)
        self.tblCustom.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.tblCustom.setSizeAdjustPolicy(QtWidgets.QAbstractScrollArea.AdjustToContents)
        self.tblCustom.setShowGrid(True)
        self.tblCustom.setRowCount(0)
        self.tblCustom.setColumnCount(2)
        self.tblCustom.setObjectName("tblCustom")
        self.verticalLayout.addWidget(self.tblCustom)
        self.pltCustom = PlotWidget(CustomDialogBase)
        self.pltCustom.setObjectName("pltCustom")
        self.verticalLayout.addWidget(self.pltCustom)
        self.wdgSouth = QtWidgets.QWidget(CustomDialogBase)
        self.wdgSouth.setObjectName("wdgSouth")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.wdgSouth)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        spacerItem = QtWidgets.QSpacerItem(217, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.btnCancel = QtWidgets.QPushButton(self.wdgSouth)
        self.btnCancel.setObjectName("btnCancel")
        self.horizontalLayout.addWidget(self.btnCancel)
        self.btnAdd = QtWidgets.QPushButton(self.wdgSouth)
        self.btnAdd.setDefault(True)
        self.btnAdd.setObjectName("btnAdd")
        self.horizontalLayout.addWidget(self.btnAdd)
        self.verticalLayout.addWidget(self.wdgSouth)

        self.retranslateUi(CustomDialogBase)
        QtCore.QMetaObject.connectSlotsByName(CustomDialogBase)

    def retranslateUi(self, CustomDialogBase):
        _translate = QtCore.QCoreApplication.translate
        CustomDialogBase.setWindowTitle(_translate("CustomDialogBase", "Neue Fuzzy-Zahl (Benutzerdefiniert)"))
        self.lblName.setText(_translate("CustomDialogBase", "<html><head/><body><p align=\"right\"><span style=\" font-size:10pt; font-weight:600;\">Name:</span></p></body></html>"))
        self.btnCancel.setText(_translate("CustomDialogBase", "Abbrechen"))
        self.btnAdd.setText(_translate("CustomDialogBase", "Hinzufügen"))

from pyqtgraph import PlotWidget
import Resource_rc
